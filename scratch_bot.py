from discord.ext import commands
import top_secret
import not_secret

bot = commands.Bot(command_prefix=commands.when_mentioned_or(not_secret.BOT_PREFIX), owner_id=not_secret.BOT_OWNER_ID)
bot.remove_command('help')


@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    await do_set_up()


@bot.group(pass_context=True)
async def set(ctx):
    if ctx.invoked_subcommand is None:
        print('Invalid sub-command used: {}'.format(ctx.message.content))


async def do_set_up():

    print('Starting set-up')

    # grab bot's home guild
    guild = bot.get_guild(not_secret.BOT_HOME_GUILD_ID)

    log_channel = None
    suggestion_channel = None

    # loop through channels and look for specific channels
    # create them if they aren't found.
    for text_channel in guild.text_channels:

        if text_channel.name == not_secret.BOT_LOG_NAME:
            log_channel = text_channel
            print('{} found. Do not create.'.format(text_channel.name))

        if text_channel.name == not_secret.BOT_SUGGESTIONS_NAME:
            suggestion_channel = text_channel
            print('{} found. Do not create.'.format(text_channel.name))

    _log_channel = await guild.create_text_channel(not_secret.BOT_LOG_NAME) if log_channel is None else log_channel

    if log_channel is None:
        await _log_channel.send('Created #{}'.format(_log_channel))

    if suggestion_channel is None:
        _suggestion_channel = await guild.create_text_channel(not_secret.BOT_SUGGESTIONS_NAME)
        await _log_channel.send('Created #{}'.format(_suggestion_channel))
    print('Done with set-up')


@set.command(aliases=['up'], pass_context=True)
async def set_up(ctx):
    await do_set_up()


@set.command(aliases=['down'], pass_context=True)
async def set_down(ctx):

    guild = bot.get_guild(not_secret.BOT_HOME_GUILD_ID)
    for text_channel in guild.text_channels:
        if text_channel.name == not_secret.BOT_LOG_NAME or text_channel.name == not_secret.BOT_SUGGESTIONS_NAME:
            print('Deleting channel #{}'.format(text_channel.name))
            await text_channel.delete()


@bot.command(pass_context=True, delete_after=3)
async def clean(ctx, num=1):

    # self explanatory
    await ctx.message.channel.purge(limit=num)


bot.run(top_secret.BOT_TOKEN)
